﻿using UnityEngine;
using System.Collections;

public class triggerNewFence : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "sheep")
        {

            Instantiate(Resources.Load("Fence"), transform.position, Quaternion.identity);
        }
    }
}
