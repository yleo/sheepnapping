﻿using UnityEngine;
using System.Collections;

public class SheepBehaviour : MonoBehaviour {
    Animator anim;
    Rigidbody rb;
    bool hungry;
    bool running;
    float timerForEating;
    float timerForWalking;
    Vector2 newWayPoint;
    Vector3 oldWayPoint;
    Vector3 wayPoint;
    float distanceFromPlayer;
    private float time = 0;

    public Transform spaceShip;
    public float speed;
    public float MinDistanceFromPlayer;
    public float MinTimeForEating;
    public float MaxTimeForEating;
    public float MinTimeForWalking;
    public float MaxTimeForWalking;
    public float WanderDistanceArea;
    public float TimeSmooth;
    public bool isUsingNavMesh;
    public float navMeshTimer;
	GameObject mPlayer;

    // Use this for initialization
    void Awake ()
    {
		mPlayer = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        timerForEating = Random.Range(MinTimeForEating, MaxTimeForEating);
        timerForWalking = Random.Range(MinTimeForWalking, MaxTimeForWalking);
        //new waypoint in a unit circle of size area
        setNewWayPoint();
		spaceShip = mPlayer.transform;
    }

	// Update is called once per frame
	void Update ()
    {
        if(navMeshTimer > 200000 && navMeshTimer < 2000005)
        {
            navMeshTimer += 200000;
        }
        if(navMeshTimer <= 0)
        {
            isUsingNavMesh = true;
        }
        if (isUsingNavMesh == false)
        {
            navMeshTimer -= Time.deltaTime;
            if (transform.position.y < .2f)
            {
                //first we should check the distance from sheep to spaceship
                distanceFromPlayer = (transform.position - spaceShip.position).magnitude;
                if (distanceFromPlayer < MinDistanceFromPlayer)
                {
                    //actions for running sheep AI
                    SheepRun();
                }
                else
                {
                    anim.SetBool("SpaceShipClose", false);
                    if (hungry)
                    {
                        SheepEat();
                    }
                    else
                    {
                        SheepWander();
                    }
                }
            }
            else
            {
                rb.AddForce(-Vector3.up * 980f);
            }
        }
	}
    void SheepRun()
    {
        anim.SetBool("SpaceShipClose", true);
        Vector3 moveTo = transform.forward;
        //moveTo.y = 0;
        rb.MovePosition(transform.position + moveTo * Time.deltaTime * speed);

        if(distanceFromPlayer < MinDistanceFromPlayer - 3)
        {
            setNewWayPoint();
        }
    }
    void SheepEat()
    {
        timerForEating = timerForEating - Time.deltaTime;
        anim.SetBool("Hungry", true);

        if(timerForEating <= 0)
        {
            anim.SetBool("Hungry", false);
            hungry = false;
            timerForEating = Random.Range(MinTimeForEating, MaxTimeForEating);
        }
    }
    void SheepWander()
    {
        timerForWalking = timerForWalking - Time.deltaTime;
        Vector3 SmoothLookAt = Vector3.Slerp(oldWayPoint, wayPoint, time/TimeSmooth);
        time += Time.deltaTime;
        SmoothLookAt.y = wayPoint.y;
        if (Vector3.Distance(transform.position, wayPoint) > WanderDistanceArea/1.8f)
        {
            transform.LookAt(SmoothLookAt);
            Vector3 moveTo = transform.forward;
            //moveTo.y = 0;
            rb.MovePosition(transform.position + moveTo * Time.deltaTime);
        }
        else
        {
            setNewWayPoint();
            transform.LookAt(SmoothLookAt);
            time = 0;
        }
        if (timerForWalking <= 0)
        {
            hungry = true;
            timerForWalking = Random.Range(MinTimeForWalking, MaxTimeForWalking);
        }

    }
    void setNewWayPoint()
    {
        newWayPoint = Random.insideUnitCircle * WanderDistanceArea;
        oldWayPoint = wayPoint;
        wayPoint = new Vector3(newWayPoint.x, wayPoint.y, newWayPoint.y);
    }
}
