﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndGameSpaceCredit : MonoBehaviour {

	public static int spaceCredit;
	public static int totalCredit;
	public static int bonusCredit;
	Text mTotalCredit;
	Text mSpaceCredit;
	Text mBonusCredit;
	TotalSpaceCredit mCredit;


	// Use this for initialization
	void Start () {
		mTotalCredit = GameObject.FindGameObjectWithTag("TotalSpaceCredit").GetComponent<Text>();
		mSpaceCredit = GameObject.FindGameObjectWithTag ("SpaceCredit").GetComponent<Text> ();
		mBonusCredit = GameObject.FindGameObjectWithTag ("Bonus").GetComponent<Text> ();
		//spaceCredit = totalCredit = bonusCredit = 0;
		spaceCredit = mCredit.getCredit ();
	}
	
	// Update is called once per frame
	void Update () {
		mTotalCredit.text = "Total Credit : " + totalCredit.ToString();
		mSpaceCredit.text = "Space Credit : " + spaceCredit.ToString();
		mBonusCredit.text = "Bonus Credit : " + bonusCredit.ToString();
	}

	public void addSpaceCredit(int spaceCredit){
		spaceCredit = spaceCredit;
	}
	public void addBonusCredt(int bonusCredit){
		bonusCredit = bonusCredit;
	}
}
