﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level3 : MonoBehaviour
{
	//GAME BALANCE - LEVEL4

    [SerializeField]
    int sheep;
    [SerializeField]
    int goldenSheep;
    public static int totalSheepCount;
    int totalPuzzleCrop;
    GameObject goldSheep; 
    Vector3 mGoldPosition; 
    Quaternion mGoldRotation;
	GameObject mGoldenSheep, mAltShip, mAltShip2;
	GameObject mCamera, mRadar;
	GameObject mTargetMe , mPlayer;
    Text sheepCountText;

    void Awake()
    {
		mCamera = GameObject.Find ("Main Camera");
		mRadar = GameObject.Find ("RadarMap");
		
		sheepCountText = GameObject.FindGameObjectWithTag("sheepCounter").GetComponent<Text>();
		if (GameControl.control.selectedShip == 0) {
			mAltShip = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip2.SetActive(false);
			mPlayer = GameObject.Find("Player");
		}
		if (GameControl.control.selectedShip == 1) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mPlayer = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		if (GameControl.control.selectedShip == 2) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("Gold_Carat_UFO_Player");
			mPlayer = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		
		mTargetMe = GameObject.FindGameObjectWithTag ("targetMe");
		mRadar.GetComponent<RadarFollow> ().target = mPlayer.transform;
		mCamera.GetComponent<RadarFollow>().target = mTargetMe.transform;
    }

    // Use this for initialization
    void Start()
    {
        totalPuzzleCrop = 112;
        goldSheep = GameObject.FindGameObjectWithTag("gold");
        mGoldPosition = new Vector3(10.0f, 10.0f, -10.0f);
        mGoldRotation = goldSheep.transform.rotation;
     
    }

    // Update is called once per frame
    void Update()
    {
        countSheep();
        sheepCountText.text = totalSheepCount.ToString();

        if (totalSheepCount == 0)
        {
			Debug.Log("TOTAL SHEEP");
			GameControl.control.isDoneLevel4 = true;
			Debug.Log("TOTAL SHEEP11");
			GameControl.control.Save();
			Debug.Log("TOTAL SHEEP22");
			Application.LoadLevel(4);
			Debug.Log("TOTAL SHEEP33");
        }

        if(totalPuzzleCrop == 0)
        {
			Debug.Log("TOTAL SHEEP44");
            GameObject clone;
            clone = Instantiate(goldSheep, mGoldPosition, mGoldRotation) as GameObject;
            clone.tag = "gold";
            totalPuzzleCrop = -1;
        }
    }

    private void countSheep()
    {
        totalSheepCount = GameObject.FindGameObjectsWithTag("sheep").Length;
    }

    public void DecrementPuzzleCounter()
    {
        totalPuzzleCrop--;
    }

}
