﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Level4 : MonoBehaviour {

	//GAME BALANCE - LEVEL5

    [SerializeField]
    int sheep;
    [SerializeField]
    int goldenSheep;
    public static int totalSheepCount;
	GameObject mCamera, mRadar;
	GameObject mTargetMe , mPlayer, mAltShip, mAltShip2;
    GameObject mGoldenSheep;
    Text sheepCountText;
    void Awake ()
    {
        sheepCountText = GameObject.FindGameObjectWithTag("sheepCounter").GetComponent<Text>();
        mGoldenSheep = GameObject.FindGameObjectWithTag("gold");
        sheep = 6;
        goldenSheep = 1;
        totalSheepCount = 0;
		mCamera = GameObject.Find ("Main Camera");
		mRadar = GameObject.Find ("RadarMap");
		
		sheepCountText = GameObject.FindGameObjectWithTag("sheepCounter").GetComponent<Text>();
		if (GameControl.control.selectedShip == 0) {
			mAltShip = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip2.SetActive(false);
			mPlayer = GameObject.Find("Player");
		}
		if (GameControl.control.selectedShip == 1) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mPlayer = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		if (GameControl.control.selectedShip == 2) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("Gold_Carat_UFO_Player");
			mPlayer = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		
		mTargetMe = GameObject.FindGameObjectWithTag ("targetMe");
		mRadar.GetComponent<RadarFollow> ().target = mPlayer.transform;
		mCamera.GetComponent<RadarFollow>().target = mTargetMe.transform;

    }
	
	// Update is called once per frame
	void Update ()
    {
        countSheep();
        sheepCountText.text = totalSheepCount.ToString();

        if (totalSheepCount-1 == 0)
        {
            
            //will save info
            GameControl.control.isDoneLevel5 = true;
            GameControl.control.Save();
			Application.LoadLevel(4);
        }

    }


    private void countSheep()
    {
        totalSheepCount = GameObject.FindGameObjectsWithTag("sheep").Length;
    }
}
