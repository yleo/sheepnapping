﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level5 : MonoBehaviour
{
	//GAME BALANCE - LEVEL3
	
	[SerializeField]
	int sheep;
	[SerializeField]
	int goldenSheep;
	public static int totalSheepCount;	
	GameObject mGoldenSheep, mAltShip, mAltShip2;
	GameObject mCamera, mRadar;
	GameObject mTargetMe , mPlayer;
	Text sheepCountText;
	
	void Awake()
	{
		mCamera = GameObject.Find ("Main Camera");
		mRadar = GameObject.Find ("RadarMap");
		
		sheepCountText = GameObject.FindGameObjectWithTag("sheepCounter").GetComponent<Text>();
		if (GameControl.control.selectedShip == 0) {
			mAltShip = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip2.SetActive(false);
			mPlayer = GameObject.Find("Player");
		}
		if (GameControl.control.selectedShip == 1) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mPlayer = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		if (GameControl.control.selectedShip == 2) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("Gold_Carat_UFO_Player");
			mPlayer = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		
		mTargetMe = GameObject.FindGameObjectWithTag ("targetMe");
		mRadar.GetComponent<RadarFollow> ().target = mPlayer.transform;
		mCamera.GetComponent<RadarFollow>().target = mTargetMe.transform;
	}
	
	// Use this for initialization
	void Start()
	{
		sheep = 10;
		goldenSheep = 1;
	}
	
	// Update is called once per frame
	void Update()
	{
		countSheep();
		sheepCountText.text = totalSheepCount.ToString();
		
		if (totalSheepCount == 0)
		{
			GameControl.control.isDoneLevel3 = true;
			GameControl.control.Save();
			Application.LoadLevel(4);

		}
	}
	
	private void countSheep()
	{
		totalSheepCount = GameObject.FindGameObjectsWithTag("sheep").Length;
	}
	
}
