﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Level1 : MonoBehaviour {

    [SerializeField]
    int sheep;
    [SerializeField]
    int goldenSheep;
    public static int totalSheepCount;
    bool bIntro, bObjective, bControls, bFuel, bStartGame, bGoldSheep;
    GameObject mIntro, mObjective, mControls, mFuel, mGoldSheepTutorial, mSpaceBar, mESC, mAlien, mPlayer, mBeam, mAltShip, mAltShip2;
	GameObject mGoldenSheep;
    Text sheepCountText;
	GameObject mCamera, mRadar;
	GameObject mTargetMe;

    void Awake()
	{	
		mCamera = GameObject.Find ("Main Camera");
		mRadar = GameObject.Find ("RadarMap");

        sheepCountText = GameObject.FindGameObjectWithTag("sheepCounter").GetComponent<Text>();
		bObjective = bControls = bFuel = bStartGame = bGoldSheep = false;
		if (GameControl.control.selectedShip == 0) {
			mAltShip = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip2.SetActive(false);
			mPlayer = GameObject.Find("Player");
		}
		if (GameControl.control.selectedShip == 1) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("LV_Diamond_UFO_Player");
			mPlayer = GameObject.Find("Gold_Carat_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}
		if (GameControl.control.selectedShip == 2) {
			mAltShip = GameObject.Find("Player");
			mAltShip2 = GameObject.Find("Gold_Carat_UFO_Player");
			mPlayer = GameObject.Find("LV_Diamond_UFO_Player");
			mAltShip.SetActive(false);
			mAltShip2.SetActive(false);
		}

		mTargetMe = GameObject.FindGameObjectWithTag ("targetMe");
		mRadar.GetComponent<RadarFollow> ().target = mPlayer.transform;
		mCamera.GetComponent<RadarFollow>().target = mTargetMe.transform;

    }

	// Use this for initialization
	void Start () {
		mGoldenSheep = GameObject.FindGameObjectWithTag("gold");
        bIntro = true;       
        mPlayer.GetComponent<CharacterBehaviour>().enabled = false;
        mBeam = GameObject.Find("BeamEmitter");
        mBeam.GetComponent<LightBeam>().enabled = false;
        sheep = 5;
        goldenSheep = 1;
        totalSheepCount = 0;
		mIntro = GameObject.Find("t_Intro");
        mIntro.SetActive(true);
		mObjective = GameObject.Find ("t_Tutorial");
        mObjective.SetActive(false);
		mControls = GameObject.Find("t_Controls");
        mControls.SetActive(false);
		mFuel = GameObject.Find("t_Fuel");
        mFuel.SetActive(false);
		mGoldSheepTutorial = GameObject.Find("t_goldSheepTutorial");
        mGoldSheepTutorial.SetActive(false);
		mSpaceBar = GameObject.Find("t_SpaceBar");
        mSpaceBar.SetActive(true);
		mESC = GameObject.Find("t_ESC");
        mESC.SetActive(true);
        mAlien = GameObject.Find("Alien");
        mAlien.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
        countSheep();
        sheepCountText.text = totalSheepCount.ToString();
		if (Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.JoystickButton1)){
            if (bObjective == true && bIntro == true && bStartGame == false && bControls == true && bFuel == true && bGoldSheep == true)
            {
                mGoldSheepTutorial.SetActive(false);
                mFuel.SetActive(false);
                mSpaceBar.SetActive(false);
                mAlien.SetActive(false);
				mPlayer.SetActive(true);
                mPlayer.GetComponent<CharacterBehaviour>().enabled = true;
				mBeam.GetComponent<LightBeam>().enabled = true;
                bStartGame = true;
            }
            if (bObjective == true && bIntro == true && bStartGame == false && bControls == true && bFuel == true && bGoldSheep == false)
            {
                mGoldSheepTutorial.SetActive(false);
                mFuel.SetActive(false);
                mSpaceBar.SetActive(false);
                mESC.SetActive(false);
                mAlien.SetActive(false);
                mPlayer.GetComponent<CharacterBehaviour>().enabled = true;
				mBeam.GetComponent<LightBeam>().enabled = true;
                bStartGame = true;
            }
            if (bObjective == true && bIntro == true && bStartGame == false && bControls == true && bFuel == false && bGoldSheep == false)
            {
                mControls.SetActive(false);
                mFuel.SetActive(true);
                bFuel = true;
            }
            if (bObjective == true && bIntro == true && bStartGame == false && bControls == false && bFuel == false && bGoldSheep == false)
            {
                mObjective.SetActive(false);
                mControls.SetActive(true);
                bControls = true;
            }
            if (bIntro == true && bStartGame == false && bObjective == false && bControls == false && bFuel == false && bGoldSheep == false)
            {
                mIntro.SetActive(false);
                mObjective.SetActive(true);
                bObjective = true;
            }
        }
		if (Input.GetKeyDown(KeyCode.Escape)||Input.GetKeyDown(KeyCode.JoystickButton0))
        {
            bObjective = true;
            bControls = true;
            bFuel = true;
            bStartGame = true;
            mIntro.SetActive(false);
            mObjective.SetActive(false);
            mControls.SetActive(false);
            mFuel.SetActive(false);
            mGoldSheepTutorial.SetActive(false);
            mSpaceBar.SetActive(false);
            mESC.SetActive(false);
            mAlien.SetActive(false);
			mPlayer.SetActive(true);
            mPlayer.GetComponent<CharacterBehaviour>().enabled = true;
            mBeam.GetComponent<LightBeam>().enabled = true;
        }
        if (mGoldenSheep == null && bObjective == true && bIntro == true && bStartGame == true && bControls == true && bFuel == true && bGoldSheep == false)
        {  
			GameObject beam = GameObject.Find("LightBeam(Clone)");
			Destroy(beam);
			bGoldSheep = true;
			mPlayer.SetActive(false);
            mPlayer.GetComponent<CharacterBehaviour>().enabled = false;
            mAlien.SetActive(true);
            mGoldSheepTutorial.SetActive(true);
            mSpaceBar.SetActive(true);
            bStartGame = false;
		}
        if (totalSheepCount == 0)
        {
			//will save info
			GameControl.control.isDoneLevel1 = true;
			GameControl.control.Save();
			Application.LoadLevel(4);
			
        }
	}

    private void countSheep()
    {
        totalSheepCount = GameObject.FindGameObjectsWithTag("sheep").Length;
    }
	
}
