﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {

    public Transform teleportTo;
    public bool isActivated;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {

            other.transform.position = teleportTo.position;
            other.GetComponent<FuelManager>().currentFuel += 25;
        }
    }
}
