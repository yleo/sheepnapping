﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpaceCredit : MonoBehaviour {

    public static int spaceCredit;
    public static int totalCredit;
    public static int bonusCredit;
	public static int levelTotalCredit;
    Text mTotalCredit;
	Text mSpaceCredit;
	Text mBonusCredit;
	private static SpaceCredit _instance;

	void Awake()
	{
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
		if (Application.loadedLevelName == "GameManager") {
			mTotalCredit = GameObject.FindGameObjectWithTag("TotalSpaceCredit").GetComponent<Text>();
			totalCredit = GameControl.control.spaceCredits;
		}
		if (Application.loadedLevelName == "Level1" || Application.loadedLevelName == "Level2" || Application.loadedLevelName == "Level3" || Application.loadedLevelName == "Level4" || Application.loadedLevelName == "Level5") {
			mSpaceCredit = GameObject.FindGameObjectWithTag("SpaceCredit").GetComponent<Text>();
			if (GameControl.control.spaceCredits > 0) {
				spaceCredit = bonusCredit = levelTotalCredit = 0;
			}
			//else initialize all to zero
			else{
				spaceCredit = totalCredit = bonusCredit = 0;
			}
		}
		if (Application.loadedLevelName == "EndGameScreen") {
			mSpaceCredit = GameObject.FindGameObjectWithTag("SpaceCredit").GetComponent<Text>();
			mBonusCredit = GameObject.FindGameObjectWithTag("Bonus").GetComponent<Text>();
			mTotalCredit = GameObject.FindGameObjectWithTag("TotalSpaceCredit").GetComponent<Text>();
			totalCredit = GameControl.control.spaceCredits;
		}
	}

	// Update is called once per frame
	void Update ()
    {
		if (Application.loadedLevelName == "GameManager") {
			mTotalCredit.text = "Space Credit : " + totalCredit.ToString();
		}
		if (Application.loadedLevelName == "Level1" || Application.loadedLevelName == "Level2" || Application.loadedLevelName == "Level3" || Application.loadedLevelName == "Level4" || Application.loadedLevelName == "Level5") {
			mSpaceCredit.text = "Space Credit : " + spaceCredit.ToString();
		}
		if (Application.loadedLevelName == "EndGameScreen") {
			mSpaceCredit.text = "Space Credit : " + spaceCredit.ToString();
			mBonusCredit.text = "Bonus Credit : " + bonusCredit.ToString();
			levelTotalCredit = spaceCredit + bonusCredit;
			mTotalCredit.text = "Total Credit : " + levelTotalCredit.ToString();

			Debug.Log("SC" + spaceCredit);
			Debug.Log("Bonus" + bonusCredit);
			Debug.Log("Total" + totalCredit);
		}
	}
    public void addCredit(int x)
    {
        spaceCredit = spaceCredit + x;
		addTotal (x);
    }
    public void addBonus(int x)
    {
        bonusCredit = bonusCredit + x;
		addTotal (x);
    }
	public void addTotal(int x){
		totalCredit = totalCredit + x;
		GameControl.control.spaceCredits = totalCredit;
	}
}
