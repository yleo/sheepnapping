﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	GameObject mlockedCanvas;
	bool bLocked;
	int mTime;
	// Use this for initialization
	void Start () {
		mlockedCanvas = GameObject.Find ("UnlockedCanvas");
		mlockedCanvas.SetActive (false);
		bLocked = false;
		mTime = 0;
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { // if left button pressed...
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                // the object identified by hit.transform was clicked
                // do whatever you want
                Debug.Log(hit.transform);
                if (hit.transform.name == "sheep1")
                {
                    GameControl.control.Save();
                    Application.LoadLevel(3);
                }
				if(hit.transform.name == "sheep2" && GameControl.control.isDoneLevel1 == true){
					GameControl.control.Save();
					Application.LoadLevel(5);
				}
				if(hit.transform.name == "sheep3" && GameControl.control.isDoneLevel2 == true){
					GameControl.control.Save ();
					Application.LoadLevel(7);
				}
				if(hit.transform.name == "sheep4" && GameControl.control.isDoneLevel3 == true){
					GameControl.control.Save();
					Application.LoadLevel(8);
				}
				if(hit.transform.name == "sheep5" && GameControl.control.isDoneLevel4 ==true){
					GameControl.control.Save ();
					Application.LoadLevel(9);
				}
				if(hit.transform.name == "store"){
					GameControl.control.Save ();
					//TODO: Leo N. Change this to store scene #
					Application.LoadLevel("UfoSelection");
				}
				if(hit.transform.name == "sheep2" && GameControl.control.isDoneLevel1 == false && bLocked == false){
					bLocked = true;
					activeCanvas();
					mTime = 0;
				}
				if(hit.transform.name == "sheep3" && GameControl.control.isDoneLevel2 == false && bLocked == false){
					bLocked = true;
					activeCanvas();
					mTime = 0;
				}
				if(hit.transform.name == "sheep4" && GameControl.control.isDoneLevel3 == false && bLocked == false){
					bLocked = true;
					activeCanvas();
					mTime = 0;
				}
				if(hit.transform.name == "sheep5" && GameControl.control.isDoneLevel4 == false && bLocked == false){
					bLocked = true;
					activeCanvas();
					mTime = 0;
				}
            }

        }
		if (bLocked == true){
			if(mTime == 60){
				bLocked = false;
				deactiveCanvas();
			}
			Debug.Log(mTime);
			mTime++;
		}
    }
	public void activeCanvas(){
		mlockedCanvas.SetActive (true);
	}
	public void deactiveCanvas(){
		mlockedCanvas.SetActive (false);
	}
}
