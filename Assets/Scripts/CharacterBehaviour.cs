﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterBehaviour : MonoBehaviour {

	//public static float speed;
	public float speed;
	public float RotateSpeed;
    public float tracterbeamForce;
	//player properties
	//**********
    static SpaceCredit mSpaceCredit;
	int spaceCredits;
	public static float fuelLevel;
	float carryingCapacity;

    public Level3 levelThree;

	//***********
	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
        //mSpaceCredit = GameObject.Find("Text").GetComponent<SpaceCredit>();
        levelThree = GameObject.Find("SpaceCredit").GetComponent<Level3>();

		if (Application.loadedLevelName == "Level3" || Application.loadedLevelName == "Level4" ) {
			mSpaceCredit = GameObject.FindGameObjectWithTag ("SpaceCredit").GetComponent<SpaceCredit>();
		} else {
			mSpaceCredit = GameObject.Find("Text").GetComponent<SpaceCredit>();
		}
	}

	// Update is called once per frame
	void Update ()
	{
		Move ();
		Beam ();
        rb.angularVelocity = Vector3.zero;
	}
	void OnCollisionEnter(Collision other)
	{
		if (other.collider.tag == "sheep")
		{
			Destroy(other.gameObject);
            mSpaceCredit.addCredit(10);
		}
        if (other.collider.tag == "gold")
        {
            Destroy(other.gameObject);
            mSpaceCredit.addBonus(50);
        }
				if(other.collider.tag ==  "Fuel")
        {
            Destroy(other.gameObject);
            GetComponent<FuelManager>().currentFuel += 25;
        }
	}

	void Move()
	{
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontal, 0.0f, vertical);
        if (horizontal != 0.0f || vertical != 0.0f)
        {
            rb.velocity = direction * speed;
            transform.rotation = Quaternion.Euler(direction.z * RotateSpeed, 0.0f, direction.x * -RotateSpeed);
        }
        //set height back to normal after being rotated and transformed
        transform.position = new Vector3(transform.position.x, 5.0f, transform.position.z);
    }
	void Beam()
	{
		if (Input.GetKey ("space") || Input.GetKey(KeyCode.JoystickButton1))
		{
			RaycastHit hitObject0;
			if (Physics.SphereCast(transform.position, 1.0f, -transform.up, out hitObject0))
			{
				if(hitObject0.transform.tag == "sheep" || hitObject0.transform.tag == "gold")
				{
                    hitObject0.rigidbody.AddForce(Vector3.up * tracterbeamForce * Time.deltaTime * 1000);
					hitObject0.transform.Rotate(new Vector3(0.0f, 1000.0f, 0.0f) * Time.deltaTime);
					if(hitObject0.transform.gameObject.GetComponent<NavMeshAgent>() != null)
					{
							Destroy(hitObject0.transform.gameObject.GetComponent<NavMeshAgent>());
					}
				}
                if (hitObject0.transform.tag == "Crop")
                {
                    Destroy(hitObject0.transform.gameObject);
                }
                if (hitObject0.transform.tag == "CropPuzzle")
                {
                    levelThree.DecrementPuzzleCounter();
                    Destroy(hitObject0.transform.gameObject);
                }
				if(hitObject0.transform.tag == "Fuel")
                {
					hitObject0.rigidbody.isKinematic = false;
                    hitObject0.rigidbody.AddForce(Vector3.up * tracterbeamForce * Time.deltaTime * 1000);
                }
			}
		}
	}
}
