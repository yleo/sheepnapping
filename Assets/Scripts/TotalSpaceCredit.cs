﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TotalSpaceCredit : MonoBehaviour {

	public static int totalSpaceCredit;
	public static int spaceCredit;
	private static TotalSpaceCredit _instance;
	Text mTotalSpaceCredit;

	void Awake()
	{
		if (!_instance)
		{
			_instance = this;
			mTotalSpaceCredit = GameObject.FindGameObjectWithTag("TotalSpaceCredit").GetComponent<Text>();
		}
		else
		{
			Destroy(this.gameObject);
		}
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
		//if new game start with zero
		if (this) {
			totalSpaceCredit = 0;
		}
		//TODO: if game is loaded, need to retrieve the saved data
		
	}
	
	// Update is called once per frame
	void Update () {
		mTotalSpaceCredit.text = "Space Credit : " + totalSpaceCredit.ToString();
		Debug.Log (spaceCredit);
	}

	public void addToTotalSC(int levelSpaceCredit){
		totalSpaceCredit = totalSpaceCredit + levelSpaceCredit;
	}
	public void trackSpaceCredit(int nSpaceCredit){
		spaceCredit = nSpaceCredit;
	}
	public int getCredit(){
		return spaceCredit;
	}
	public void resetSpaceCredit(){
		spaceCredit = 0;
	}
}
