﻿using UnityEngine;
using System.Collections;

public class NavigateToo : MonoBehaviour {

    public Transform target;
    NavMeshAgent agent;
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (GetComponent<SheepBehaviour>().isUsingNavMesh)
        {
            agent.SetDestination(target.position);
        }
	
	}
}
