﻿using UnityEngine;
using System.Collections;

public class GMSheepAnim : MonoBehaviour 
{
    Animator anim;
    [SerializeField]
    int animInt;

	// Use this for initialization
	void Start () 
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        anim.SetInteger("SetInt", animInt);
	}
}
