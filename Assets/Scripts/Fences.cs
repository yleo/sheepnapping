﻿using UnityEngine;
using System.Collections;

public class Fences : MonoBehaviour {

    Rigidbody mRigidBody;

    void Awake()
    {
        mRigidBody = GetComponent<Rigidbody>();
    }

	// Use this for initialization
	void Start () {
        mRigidBody.isKinematic = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
