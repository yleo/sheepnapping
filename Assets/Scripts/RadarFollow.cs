﻿using UnityEngine;
using System.Collections;

public class RadarFollow : MonoBehaviour {

	public Transform target;
	public float m_smooth;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 newTarget = new Vector3 (target.position.x, transform.position.y, target.position.z);
		transform.position = Vector3.Lerp (transform.position, newTarget, Time.deltaTime * m_smooth);
	
	}
}
