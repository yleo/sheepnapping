﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FuelManager : MonoBehaviour 
{

	bool isMoving;
	bool isAbducting;

	public Image bar;
	public float maxFuel = 100.0f;
	public float currentFuel = 0.0f;

	void Start() 
	{
	
		CharacterBehaviour.fuelLevel = maxFuel;
		currentFuel = maxFuel;
		//InvokeRepeating ("decreaseFuel", 0.0f, 2.0f);

	}

	void decreaseFuel() 
	{
		currentFuel -= 5.0f;
		float calculateFuel = currentFuel / maxFuel;
		SetFuel (calculateFuel);
	}

	void SetFuel(float fuel) 
	{
		CharacterBehaviour.fuelLevel = fuel;
		bar.fillAmount = fuel;
	}

	private bool playerMoving() 
	{
		if (Input.GetAxis ("Horizontal") != 0 || Input.GetAxis ("Vertical") != 0) 
		{
			return true;
		} 
		else
		{
			return false;
		}
	}

	private bool playerAbducting()
	{
		if (Input.GetKey (KeyCode.Space)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	private int counter = 0;

	// Update is called once per frame
	void Update () {

		isMoving = playerMoving ();
		isAbducting = playerAbducting ();

		if (isMoving) 
		{
			// When ufo is moving
			counter++;
		}
		if (isAbducting) 
		{
			// When ufo is abducting
			counter = counter + 3;
		}
		if (isMoving && isAbducting) 
		{
			// When ufo is moving AND abducting
			counter = counter + 5;
		}

		if (counter > 120)
		{
			counter = 0;
			decreaseFuel();

		}
        if (currentFuel < 0.0f)
        {
            Application.LoadLevel(4);
        }

		if (CharacterBehaviour.fuelLevel < 0) 
		{
			// The player loses, redirect to a game over screen
			Debug.Log("Game Over");
		}


	
	}

}
