﻿using UnityEngine;
using System.Collections;

public class LightBeam : MonoBehaviour
{
    Animator mAnimator;
    bool mShooting;

    float kShootDuration = 0.25f;
    float mTime;

    [SerializeField]
    GameObject mBeam;
    CharacterBehaviour mShip;

    AudioSource mBusterSound;

    void Start()
    {
        mAnimator = transform.parent.GetComponent<Animator>();
		if (GameControl.control.selectedShip == 0) {
			mShip = GameObject.Find("Player").GetComponent<CharacterBehaviour>();
		}
		if (GameControl.control.selectedShip == 1) {
			mShip = GameObject.Find("Gold_Carat_UFO_Player").GetComponent<CharacterBehaviour>();
		}
		if (GameControl.control.selectedShip == 2) {
			mShip = GameObject.Find("LV_Diamond_UFO_Player").GetComponent<CharacterBehaviour>();
		}



    }

    void Update()
    {
		if (Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.JoystickButton1) )
        {
            GameObject clone;
            clone = Instantiate(mBeam, transform.position, transform.rotation) as GameObject;
			if (GameControl.control.selectedShip == 0) {
				clone.transform.parent = GameObject.Find("Player").transform;
			}
			if (GameControl.control.selectedShip == 1) {
				clone.transform.parent = GameObject.Find("Gold_Carat_UFO_Player").transform;
			}
			if (GameControl.control.selectedShip == 2) {
				clone.transform.parent = GameObject.Find("LV_Diamond_UFO_Player").transform;
			}
         
        }
        if (Input.GetKeyUp("space") || Input.GetKeyUp(KeyCode.JoystickButton1))
        {

            Destroy(GameObject.FindGameObjectWithTag("LightBeam"));
        }
     
    }
}
