﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UfoNameManager : MonoBehaviour {

	Text name;

	// Use this for initialization
	void Awake () {
	
		name = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (RotatingBehaviour.currentSelection == 0) 
		{
			name.text = "The Black UFO";
		}
		if (RotatingBehaviour.currentSelection == 1) 
		{
			name.text = "The Gold UFO";	
		}
		if (RotatingBehaviour.currentSelection == 2) 
		{
			name.text = "UFO by Louis Vuitton";
		}
	}
}
