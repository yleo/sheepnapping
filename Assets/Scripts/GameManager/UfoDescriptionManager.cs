﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UfoDescriptionManager : MonoBehaviour {

	Text description;

	// Use this for initialization
	void Awake () {

		description = GetComponent<Text> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (RotatingBehaviour.currentSelection == 0) 
		{
			description.text = "A simple spaceship with basic features.";
		}
		if (RotatingBehaviour.currentSelection == 1) 
		{
			description.text = "Cool and chic spaceship made 100% of gold 24-karat. Simple yet elegant.";
		}
		if (RotatingBehaviour.currentSelection == 2) 
		{
			description.text = "Internal body made of diamond. External body covered with LV signature leather and more diamonds.";
		}
	
	}
}
