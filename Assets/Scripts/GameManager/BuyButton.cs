﻿using UnityEngine;
using System.Collections;

public class BuyButton : MonoBehaviour {

	private int exceptionTimer = 0;

	// Use this for initialization
	void Start () {

		transform.GetChild (1).gameObject.SetActive (false);
	
	}
	
	// Update is called once per frame
	void Update () 
	{

		//if (RotatingBehaviour.unlocked [RotatingBehaviour.currentSelection] == false)
		if (GameControl.control.ufosUnlocked [RotatingBehaviour.currentSelection] == false)
		{
			transform.GetChild(0).gameObject.SetActive (true);
		} 
		else 
		{
			transform.GetChild(0).gameObject.SetActive (false);
		}

		if (transform.GetChild (1).gameObject.activeInHierarchy) 
		{
			exceptionTimer++;
			if(exceptionTimer == 240)
			{
				transform.GetChild (1).gameObject.SetActive (false);
				exceptionTimer = 0;
			}
		}

	}

	public void Buy () 
	{

		if (GameControl.control.spaceCredits < RotatingBehaviour.prices [RotatingBehaviour.currentSelection]) {
			// THE USER IS BROKE
			Debug.Log ("Not enough money");
			transform.GetChild (1).gameObject.SetActive (true);
		} 
		else 
		{	// Modifies amount from wallet, unlock the ufo, and save
			GameControl.control.spaceCredits = GameControl.control.spaceCredits - RotatingBehaviour.prices [RotatingBehaviour.currentSelection];
			GameControl.control.ufosUnlocked [RotatingBehaviour.currentSelection] = true;
			GameControl.control.Save ();
		}
		
	}


}
