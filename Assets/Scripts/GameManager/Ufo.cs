﻿using UnityEngine;
using System.Collections;

public class Ufo : MonoBehaviour {

	[SerializeField]
	private string mName;

	[SerializeField]
	private int mPrice;

	private bool mUnlocked;


	public string GetName()
	{
		return mName;
	}

	public int GetPrice()
	{
		return mPrice;
	}

	public bool IsUnlocked()
	{
		return mUnlocked;
	}

	public void SetLock(bool newState)
	{
		mUnlocked = newState;
	}
}
