﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WalletManager : MonoBehaviour {

	Text currentAmount;

	private int money;

	// Use this for initialization
	void Start () {

		currentAmount = GetComponent<Text> ();
		money = GameControl.control.spaceCredits;
	
	}
	
	// Update is called once per frame
	void Update () {
		getMoney ();
		currentAmount.text = money + " Space Credits";

	}

	public void getMoney(){
		money = GameControl.control.spaceCredits;
	}
}
