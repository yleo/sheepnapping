﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotatingBehaviour : MonoBehaviour {

	public static int currentSelection = 0; // default selection index

	//0: Black/Player UFO
	//1: Gold UFO
	//2: LV UFO

	float timer = 0;

	public List<GameObject> ufos;
	//public static bool [] unlocked; // true if unlocked

	public static int [] prices;

	[SerializeField]
	float speed;

	// Use this for initialization
	void Start () {

		ufos = new List<GameObject> ();
		foreach (Transform t in transform) 
		{
			ufos.Add (t.gameObject);
			t.gameObject.SetActive(false);
		}

		ufos [currentSelection].gameObject.SetActive (true);

		// Prices
		prices = new int[3];
		prices [0] = 0;
		prices [1] = 100;
		prices [2] = 500;


	}

	public void Select(int index)
	{
		if (index == currentSelection)
			return;
		if (index < 0 || index >= ufos.Count)
			return;

		ufos [currentSelection].gameObject.SetActive (false);
		currentSelection = index;
		GameControl.control.selectedShip = currentSelection;
		ufos [currentSelection].gameObject.SetActive (true);

	}
	
	// Update is called once per frame
	void Update () {

		// If hold left-click on mouse, can rotate model
		if (Input.GetMouseButton (0)) 
		{
			transform.Rotate (new Vector3 (0.0f, Input.GetAxis ("Mouse X"), 0.0f));
		}

		float step = speed * Time.deltaTime;

		// Or use arrows to rotate
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			transform.RotateAround(transform.position, transform.up, -Time.deltaTime * 50);
		}

		if(Input.GetKey(KeyCode.RightArrow))
		{
			transform.RotateAround(transform.position, transform.up, Time.deltaTime * 50);
		}


		timer++;

		// Make model float up and down
		if (timer > 0 && timer < 60) {
			transform.Translate(Vector3.up * Time.deltaTime * speed);
		}
		if (timer > 60 ){
			transform.Translate(-Vector3.up * Time.deltaTime * speed);
		}
		if (timer > 120) {
			timer = 0;
		}
	
	}

	public void GoBack()
	{
		Application.LoadLevel ("GameManager");
	}


}
