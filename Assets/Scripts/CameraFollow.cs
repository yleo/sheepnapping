﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	float kFollowSpeed = 5.5f;
	float stepOverThreshold = 1.0f;
	GameObject Following;
	void Start()
	{
		//Following = GameObject.FindGameObjectWithTag ("Player");
		if (GameControl.control.selectedShip == 0) {
			Following = GameObject.Find("Player");
		}
		if (GameControl.control.selectedShip == 1) {
			Following = GameObject.Find("Gold_Carat_UFO_Player");
		}
		if (GameControl.control.selectedShip == 2) {
			Following = GameObject.Find("LV_Diamond_UFO_Player");
		}
	}
	
	void Update ()
	{
		if(Following != null)
		{
			Vector3 targetPosition = new Vector3(Following.transform.position.x, 7.0f, Following.transform.position.z - 5.0f);
			Vector3 direction = targetPosition - transform.position;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray))
            {
                Debug.DrawRay(ray.origin, ray.direction);
            }
            transform.position = new Vector3(Input.mousePosition.x,
			                                 0.0f,
			                                 Input.mousePosition.z);


			if(direction.magnitude > stepOverThreshold)
			{
				// If too far, translate at kFollowSpeed
				transform.Translate (direction.normalized * kFollowSpeed * Time.deltaTime, Camera.main.transform);
			}
			else
			{
				// If close enough, just step over
				transform.position = targetPosition;
			}
		}
	}
}
