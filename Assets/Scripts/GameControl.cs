﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class GameControl : MonoBehaviour {
    //script is for saveing and load player data

    //player data
    public int spaceCredits;
    public bool isDoneLevel1;
    public float timeCompleteLevel1;
    public bool isDoneLevel2;
    public float timeCompleteLevel2;
    public bool isDoneLevel3;
    public float timeCompleteLevel3;
	public bool isDoneLevel4;
	public float timeCompleteLevel4;
	public bool isDoneLevel5;
	public float timeCompleteLevel5;
	public bool[] ufosUnlocked;
	public int selectedShip;
    //perhaps an array or list here?
    //reference to this class
    public static GameControl control;
    void Awake()
    {
        //singleton design pattern 
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else if(control != this)
        {
            Destroy(gameObject);
        }

		// The initial values when opened for first time
		ufosUnlocked = new bool[3];
		ufosUnlocked [0] = true;
		ufosUnlocked [1] = false;
		ufosUnlocked [2] = false;
		selectedShip = 0;

    }
    //method for saving 
    public void Save()
    {
        //new binary formatter
        BinaryFormatter bf = new BinaryFormatter();
        //create file
        FileStream file = File.Create(Application.persistentDataPath + "/playerinfo.dat");
        PlayerData data = new PlayerData();
        data.spaceCredits = spaceCredits;
        data.isDoneLevel1 = isDoneLevel1;
        data.isDoneLevel2 = isDoneLevel2;
        data.isDoneLevel3 = isDoneLevel3;
		data.isDoneLevel4 = isDoneLevel4;
		data.isDoneLevel5 = isDoneLevel5;
        data.timeCompleteLevel1 = timeCompleteLevel1;
        data.timeCompleteLevel2 = timeCompleteLevel2;
        data.timeCompleteLevel3 = timeCompleteLevel3;
		data.timeCompleteLevel4 = timeCompleteLevel4;
		data.timeCompleteLevel5 = timeCompleteLevel5;
		data.ufosUnlocked = ufosUnlocked;
		data.selectedShip = selectedShip;
        //add whatever you want to save
        bf.Serialize(file, data);
        file.Close();
    }
    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerinfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerinfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();
            spaceCredits = data.spaceCredits;
            isDoneLevel1 = data.isDoneLevel1;
            isDoneLevel2 = data.isDoneLevel2;
            isDoneLevel3 = data.isDoneLevel3;
			isDoneLevel4 = data.isDoneLevel4;
			isDoneLevel5 = data.isDoneLevel5;
            timeCompleteLevel1 = data.timeCompleteLevel1;
            timeCompleteLevel2 = data.timeCompleteLevel2;
            timeCompleteLevel3 = data.timeCompleteLevel3;
			timeCompleteLevel4 = data.timeCompleteLevel4;
			timeCompleteLevel5 = data.timeCompleteLevel5;
			ufosUnlocked = data.ufosUnlocked;
			selectedShip = data.selectedShip;
        }
    }

}

//serializable class to easily write to file
[Serializable]
class PlayerData
{
    //player's data
    public int spaceCredits;
    public bool isDoneLevel1;
    public float timeCompleteLevel1;
    public bool isDoneLevel2;
    public float timeCompleteLevel2;
    public bool isDoneLevel3;
    public float timeCompleteLevel3;
	public bool isDoneLevel4;
	public float timeCompleteLevel4;
	public bool isDoneLevel5;
	public float timeCompleteLevel5;
	public bool [] ufosUnlocked;
	public int selectedShip;
    //maybe implement in proper oop fashion
}
